<section class="information" id="page_information">
  <section class="contents_outer bg_gray">
    <?php
  $wp_query = new WP_Query();
  $param = array(
    'posts_per_page' => '-1', //表示件数。-1なら全件表示
    'post_status' => 'publish',
    'post_type' => 'seminar',
    'orderby' => 'date', //ID順に並び替え
    'order' => 'DESC'
  );
  $wp_query->query($param);?>
    <?php if($wp_query->have_posts()):?>
    <section class="seminar">
      <div class="wrapper">
        <h3 class="headline1 pt_l pb_l enter-top">Seminar<span class="small">セミナー</span></h3>
        <ul class="cf enter-bottom">
          <?php while($wp_query->have_posts()) :?>
          <?php $wp_query->the_post(); ?>
          <li class="">
            <dl class="cf">
              <dt><span class="date">
                <?php the_time('Y.m.d'); ?>
                </span></dt>
              <dd>
                <p class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php echo $post->post_title; ?></a></p>
                <div class="excerpt">
                  <?php the_excerpt(); ?>
                  <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">→詳細はこちら</a></div>
              </dd>
            </dl>
          </li>
          <?php endwhile; ?>
        </ul>
      </div>
      <!-- wrapper --> 
    </section>
    <!-- seminar -->
    
    <?php endif; ?>
    <?php wp_reset_query(); ?>
  </section>
  <!-- contents_bottom --> 
  
</section>
<!-- information --> 