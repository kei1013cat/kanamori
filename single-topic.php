<?php get_header(); ?>

<div id="contents_wrap" class="bg_gray">
<?php get_template_part('part-title'); ?>
<div class="wrapper">
	<div class="seminar" id="contents">
		<section class="news_entry pt pb_l">
            <h3 class="headline1 pt enter-top">Topic<span class="small">トピック</span></h3>
            
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article <?php post_class(); ?>>
				<div class="pt entry-header">

					<p class="pt_xs">
						<time class="entry-date" datetime="<?php the_time( 'Y/m/d' ); ?>" pubdate="<?php the_time( 'Y/m/d' ); ?>">
							<?php the_time( 'Y/m/d'  ); ?>
						</time>
					</p>
					<h3 class="headline4">
						<?php the_title(); ?>
					</h3>
				</div>
				<section class="entry-content">
                   
                    <?php the_content(); ?>

				</section>
			</article>
			<?php endwhile; endif; ?>
			<?php wp_reset_query(); ?>
		</section>
	</div>
	<!-- contents -->

</div>
<!-- wrapper -->
</div>
<?php get_footer(); ?>
