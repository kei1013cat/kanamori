<?php get_header(); ?>
<?php $separate_num = get_field('相談メッセージの区切り'); // 対談数いくつでセクションを分けるか　?>

<div id="contents_wrap" class="bg_gray">
<?php get_template_part('part-title'); ?>
    <div class="wrapper">
    	<div class="talk" id="contents">
    		<section class="news_entry pt pb_l">
                <h3 class="headline1 pt enter-top">Talk<span class="small">対談</span></h3>
    			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    			<article <?php post_class(); ?>>
    				<div class="entry-header mb pt">
                        
    					<p class="pt_xs">
    						<time class="entry-date" datetime="<?php the_time( 'Y/m/d' ); ?>" pubdate="<?php the_time( 'Y/m/d' ); ?>">
    							<?php the_time( 'Y/m/d'  ); ?>
    						</time>
    					</p>
    					<h3 class="headline4 mb_s">
    						<?php the_title(); ?>
    					</h3>
                        
    				</div>
                    
    				<section class="entry-content">
                         <?php the_content(); ?>
                    </section>
                </article>
                <?php endwhile; endif; ?>
                <?php wp_reset_query(); ?>
    		</section>
            <!-- news_entry -->
    	</div>
    	<!-- contents -->
    </div>
</div>
<?php get_footer(); ?>
