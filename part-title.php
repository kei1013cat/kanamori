<?php
$url = $_SERVER['REQUEST_URI'];
$parent_id = $post->post_parent; // 親ページのIDを取得
$parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得

?>


<?php if(is_singular( 'seminar' ) ):?>

    <div id="pagetitle" class="fead">
        <div class="bg-mask">
            <div class="pagetitle-text enter-bottom5">
                セミナー
            </div>
        </div>
    </div>

<?php elseif(is_singular( 'talk' ) ):?>

    <div id="pagetitle" class="fead">
        <div class="bg-mask">
            <div class="pagetitle-text enter-bottom5">
                対談
            </div>
        </div>
    </div>
<?php elseif(is_singular( 'topic' ) ):?>

    <div id="pagetitle" class="fead">
        <div class="bg-mask">
            <div class="pagetitle-text enter-bottom5">
                トピック
            </div>
        </div>
    </div>

<?php elseif(is_single()):?>

    <div id="pagetitle" class="fead">
        <div class="bg-mask">
            <div class="pagetitle-text enter-bottom5">
                お知らせ
            </div>
        </div>
    </div>


<?php else: ?>


    <div id="pagetitle" class="fead">
        <div class="bg-mask">
            <div class="pagetitle-text enter-bottom5">
                <?php the_title(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

