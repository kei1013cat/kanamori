<?php get_header(); ?>



<?php
$wp_query = new WP_Query();
$param = array(
'posts_per_page' => '3', //表示件数。-1なら全件表示
'post_status' => 'publish',
'orderby' => 'date', //ID順に並び替え
'order' => 'DESC'
);
$wp_query->query($param);?>

<div id="contents_wrap">
  <div id="contents">

  <section id="mainimage" class="mainimage fead">
      <div class="wrapper">
          <div class="main-outer cf">
              <div class="photo">
                  <img src="<?php bloginfo('template_url'); ?>/images/profile_photo2.png">
              </div>
              <!-- main-text -->
              <div class="main-text">
                  <h2 class="head-title enter-left">
                    労働者一人ひとりの個性に合った<br>キャリア構築支援を目指して
                  </h2>
              </div>
              <!-- main-text -->
          </div>
          <!-- main-outer -->
          <?php if($wp_query->have_posts()):?>
          <div class="news-outer cf enter-bottom2">
              <div class="news-title">NEWS</div>
              <div class="news-list cf enter-bottom1">
                  <?php while($wp_query->have_posts()) :?>
                  <?php $wp_query->the_post(); ?>
                  <dl class="cf">
                    <dt><?php the_time('Y.m.d'); ?></dt>
                    <dd>
                      <a href="<?php echo get_field('リンク');?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php echo $post->post_title; ?></a>
                    </dd>
                  </dl>
                  <?php endwhile; ?>
              </div>
          </div>
          <?php endif; ?>
          <?php wp_reset_query(); ?>
          
      </div>
      <!-- wrapper -->
  </section>
  <!-- mainimage -->

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
<?php else : ?>

<?php endif; ?>

<?php
$wp_query = new WP_Query();
$param = array(
'posts_per_page' => '3', //表示件数。-1なら全件表示
'post_status' => 'publish',
'post_type' => 'seminar',
'orderby' => 'date', //ID順に並び替え
'order' => 'DESC'
);
$wp_query->query($param);?>

<?php if($wp_query->have_posts()):?>
    <section class="contents_bottom bg_gray">
      <section class="seminar">
          <div class="wrapper">
              <h3 class="headline1 pt_l pb_l enter-top">Seminar<span class="small">セミナー</span></h3>

                  <ul class="cf enter-bottom">
                      <?php while($wp_query->have_posts()) :?>
                      <?php $wp_query->the_post(); ?>
                      <li>
                      
                      <dl class="cf">
                      <dt><span class="date"><?php the_time('Y.m.d'); ?></span></dt>
                      <dd>
                    <p class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php echo $post->post_title; ?></a></p>
                    <div class="excerpt"><?php the_excerpt(); ?><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">→詳細はこちら</a></div>
                  </dd>
                    </dl>
                    
                    </li>
                    <?php endwhile; ?>
                  </ul>
          </div>
          <!-- wrapper -->
      </section>
      <!-- seminar -->
<?php endif; ?>
<?php wp_reset_query(); ?>


<?php
$wp_query = new WP_Query();
$param = array(
'posts_per_page' => '3', //表示件数。-1なら全件表示
'post_status' => 'publish',
'post_type' => 'talk',
'orderby' => 'date', //ID順に並び替え
'order' => 'DESC'
);
$wp_query->query($param);?>

<?php if($wp_query->have_posts()):?>

      <section class="seminar">
          <div class="wrapper">
              <h3 class="headline1 pt_l pb_l enter-top">Talk<span class="small">対談</span></h3>
                  <ul class="cf enter-bottom">
                      <?php while($wp_query->have_posts()) :?>
                      <?php $wp_query->the_post(); ?>
                      <li>

                      <dl class="cf">
                      <dt><span class="date"><?php the_time('Y.m.d'); ?></span></dt>
                      <dd>
                    <p class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php echo $post->post_title; ?></a></p>
                    <div class="excerpt"><?php the_excerpt(); ?><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">→詳細はこちら</a></div>
                  </dd>
                      </dl>
                    </li>
                    <?php endwhile; ?>
                  </ul>
          </div>
          <!-- wrapper -->
      </section>
      <!-- seminar -->
  <?php endif; ?>
  <?php wp_reset_query(); ?>




<?php
$wp_query = new WP_Query();
$param = array(
'posts_per_page' => '3', //表示件数。-1なら全件表示
'post_status' => 'publish',
'post_type' => 'topic',
'orderby' => 'date', //ID順に並び替え
'order' => 'DESC'
);
$wp_query->query($param);?>

<?php if($wp_query->have_posts()):?>

      <section class="talk">
          <div class="wrapper">
              <h3 class="headline1 pt_l pb_l enter-top">Topic<span class="small">トピック</span></h3>
                  <ul class="cf enter-bottom">
                      <?php while($wp_query->have_posts()) :?>
                      <?php $wp_query->the_post(); ?>
                      <li>

                      <dl class="cf">
                      <dt><span class="date"><?php the_time('Y.m.d'); ?></span></dt>
                      <dd>
                    <p class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php echo $post->post_title; ?></a></p>
                    <div class="excerpt"><?php the_excerpt(); ?><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">→詳細はこちら</a></div>
                  </dd>
                      </dl>
                    </li>
                    <?php endwhile; ?>
                  </ul>
          </div>
          <!-- wrapper -->
      </section>
      <!-- seminar -->
  <?php endif; ?>
  <?php wp_reset_query(); ?>

        

    </section>    
    <!-- contents_bottom -->

  </div>
  <!--contents -->
</div>
<!--contents_wrap -->
<?php get_footer(); ?>
