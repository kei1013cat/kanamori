<?php get_header(); ?>

<div id="contents_wrap" class="bg_gray">
<?php get_template_part('part-title'); ?>
<div class="wrapper">
	<div class="seminar" id="contents">
		<section class="news_entry pt pb_l">
            <h3 class="headline1 pt enter-top">Seminar<span class="small">セミナー</span></h3>
            
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article <?php post_class(); ?>>
				<div class="pt entry-header">

					<p class="pt_xs">
						<time class="entry-date" datetime="<?php the_time( 'Y/m/d' ); ?>" pubdate="<?php the_time( 'Y/m/d' ); ?>">
							<?php the_time( 'Y/m/d'  ); ?>
						</time>
					</p>
					<h3 class="headline4">
						<?php the_title(); ?>
					</h3>
				</div>
				<section class="entry-content">
                   
                    <?php the_content(); ?>
<p class="linkbtn2 enter-bottom1" data-sr-id="3" style="; visibility: visible;  -webkit-transform: translateY(0) scale(1); opacity: 1;transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.1s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.1s; transition: transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.1s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.1s; "><a href="m&#97;i&#108;t&#111;:&#105;&#110;&#102;&#111;&#64;k-&#108;&#97;&#98;&#111;&#114;-&#108;&#97;&#98;&#46;&#106;&#112;" style="text-decoration:none;">お問い合わせはこちら</a></p>
				</section>
			</article>
			<?php endwhile; endif; ?>
			<?php wp_reset_query(); ?>
		</section>
	</div>
	<!-- contents -->

</div>
<!-- wrapper -->
</div>
<?php get_footer(); ?>
