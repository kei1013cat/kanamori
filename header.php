<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width">

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo '  '; }
 bloginfo('name'); ?>
</title>
<meta name="Keywords" content="金森労働研究所,教育支援,創業支援,セルフキャリアドック支援,セミナー,対談,ライフスタイル,金森史枝" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" />

<?php if(is_pc()):?>
<!--[if lt IE 9]>
    <script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
    <![endif]-->
<?php endif; ?>
<?php //以下user設定 ?>
<script src="<?php bloginfo('template_url'); ?>/js/jquery-3.3.1.min.js"></script>
<!-- scrollreveal -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.thema.js"></script>

<!-- smoothScroll -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>
<?php if(!is_ipad()):?>
<!-- pulldown -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>
<?php endif; ?>

<!-- matchHeight -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/matchHeight_userdf.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/iscroll.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/drawer/dist/css/drawer.min.css">
<script src="<?php bloginfo('template_url'); ?>/js/drawer/dist/js/drawer.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
    $('.drawer').drawer();
});

jQuery(function($) {
    // ドロワーメニューが開いたとき
    $('.drawer').on('drawer.opened', function(){
        $('#menu-text').text('CLOSE');
    });
    // ドロワーメニューが閉じたとき
    $('.drawer').on('drawer.closed', function(){
        $('#menu-text').text('MENU');
    });
});
</script>

<!-- ユーザー定義 -->
<script src="<?php bloginfo('template_url'); ?>/js/ajaxzip3.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/ajaxzip3_userdf.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137031084-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137031084-1');
</script>


<?php wp_head(); ?>
</head>
<?php
  $body_id = "";
  $body_class = "";
  if ( is_front_page() ) {
    $body_id = ' id="page_index"';
  }else if ( is_page() ) {
    if($post -> post_parent == 0 ){
        $body_id = ' id="page_'.$post->post_name.'"';
    }else{
        $ancestors =  $post-> ancestors;
        foreach($ancestors as $ancestor){
            $body_id = ' id="page_'.get_post($ancestor)->post_name.'"';
            break;
        }
    }
    $body_class = ' subpage';
    if( $post->post_parent){
        $body_class = ' '.$post->post_name;
    }
  }else if (  is_single() ) {
    $body_id = ' id="page_single"';
   $body_class = " subpage ".get_post_type( $post );
  }else if ( is_archive() ) {
    $body_id = ' id="page_archive"';
    $body_class = " subpage ".get_post_type( $post );
  }else if ( is_404() ) {
    $body_id = ' id="page_404"';
    $body_class = ' subpage';
  }
?>
<body<?php echo $body_id; ?> class="drawer drawer--top<?php echo $body_class; ?>">
<div id="outer">
<header>
<div class="enter-top1">
  <div class="pcmenu cf">
    <div class="wrapper">
    <div class="left">
      <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.png" alt="金森労働研究所 | Kanamori Labor Research Laboratory" /> </a> </h1>
    </div>
    <!-- left -->
    <div class="right cf">
      <nav>
        <ul class="cf">
          <li><a href="<?php bloginfo('url'); ?>/">ホーム<span>HOME</span></a></li>
          <li><a href="<?php bloginfo('url'); ?>/about/">金森労働研究所について<span>ABOUT US</span></a></li>
          <li><a href="<?php bloginfo('url'); ?>/seminar_list/">セミナー<span>SEMINAR</span></a></li>
          <li><a href="<?php bloginfo('url'); ?>/talk_list/">対談<span>Talk</span></a></li>
          <li><a href="<?php bloginfo('url'); ?>/topic_list/">トピック<span>Topic</span></a></li>
        </ul>
      </nav>
    </div>
    <!-- right --> 
    </div>
    <!-- wrapper -->
  </div>
  <!-- pcmenu -->
</div>
  <div class="spmenu drawermenu" role="banner" id="top">
    <h2><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.png" alt="金森労働研究所 | Kanamori Labor Research Laboratory" /></a></h2>
    <button type="button" class="drawer-toggle drawer-hamburger"><span class="sr-only">toggle navigation</span> <span class="drawer-hamburger-icon"></span>
    <div id="menu-text" class="text">MENU</div>
    </button>
    <nav class="drawer-nav" role="navigation">
      <div class="inner">
        <h3 class="sp-title">MENU</h3>
        <ul class="drawer-menu cf">
          <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/">HOME<span>ホーム</span></a></li>
          <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/about/">ABOUT US<span>金森労働研究所について</span></a></li>
          <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/seminar_list/">SEMINAR<span>セミナー</span></a></li>
          <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/talk_list/">Talk<span>対談</span></a></li>
          <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/topic_list/">Topic<span>トピック</span></a></li>
      </ul>
      </div>
      <!-- inner --> 
    </nav>

  </div>
  <!-- spmenu --> 
  
</header>
<main role="main">
