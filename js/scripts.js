$(window).scroll(headerParallax);

function headerParallax() {
	var st = $(window).scrollTop();
	var headerScroll = $('.masthead h1');

	if (st < 500) {
		headerScroll.css('opacity', 1-st/1000);
		$('.masthead-arrow ').css('opacity', 0.5-st/250);
		headerScroll.css({
			'-webkit-transform' : 'translateY(' + st/7 + '%)',
			'-ms-transform' : 'translateY(' + st/7 + '%)',
			transform : 'translateY(' + st/7 + '%)'
		});
	}
}


   $(function(){
      $(window).scroll(function(){
         if ($(this).scrollTop() >= 120) {
            $('header').addClass('fixed');
            $('#page_index #mainvisual .vegas-outer').addClass('bg');
         } else {
            $('header').removeClass('fixed');
            $('#page_index #mainvisual .vegas-outer').removeClass('bg');
         }
      });
   });
