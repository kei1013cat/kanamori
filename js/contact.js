    jQuery(function($){
    
    if(return_reload()) {

        // 戻るボタン押下時の初期値設定 ///////////////////////////////////////

        // お問い合わせ区分の設定
        if ($('input[name=contact_type_hidden]:eq(0)').prop('checked')) {
            $("#contact_type1").click();
            disable_area2();
            enable_area1();
            hover1();
        }

        if ($('input[name=contact_type_hidden]:eq(1)').prop('checked')) {
            $("#contact_type2").click();
            disable_area1();
            enable_area2();
            $("#area1").removeClass('hover');
            $("#area2").addClass('hover');
            hover2();
        }
        if ($('input[name=contact_type_hidden]:eq(2)').prop('checked')) {          
            $("#contact_type3").click();
            disable_area1();
            disable_area2();
            $("#area1").removeClass('hover');
            $("#area2").removeClass('hover');
            hover3();
        }
        
        // エリア区分の設定
        if ($('input[name=area_type_hidden]:eq(0)').prop('checked')) {
            if ($('input[name=contact_type_hidden]:eq(0)').prop('checked')) {
                $("#area1_type1").click();
            } else {
                $("#area2_type1").click();        
            }
            up_hokkaido();
        }
        
        if ($('input[name=area_type_hidden]:eq(1)').prop('checked')) {
            if ($('input[name=contact_type_hidden]:eq(0)').prop('checked')) {
                $("#area1_type2").click();
            } else {
                $("#area2_type2").click();        
//                $("input[name='area1_type']").attr("checked", false);
            }
            up_touhoku();
        }

        if ($('input[name=area_type_hidden]:eq(2)').prop('checked')) {
            if ($('input[name=contact_type_hidden]:eq(0)').prop('checked')) {
                $("#area1_type3").click();
            } else {
                $("#area2_type3").click();        
//                $("input[name='area1_type']").attr("checked", false);
            }
            up_kantou();
        }

        if ($('input[name=area_type_hidden]:eq(3)').prop('checked')) {
            if ($('input[name=contact_type_hidden]:eq(0)').prop('checked')) {
                $("#area1_type4").click();
            } else {
                $("#area2_type4").click();        
                $("input[name='area1_type']").attr("checked", false);
            }
            up_etc();
        }

        if ($('input[name=contact_type_hidden]:eq(0)').prop('checked')) {
            $("#product").html("工場製品");      
        }
        if ($('input[name=contact_type_hidden]:eq(1)').prop('checked')) {
            $("#product").html("杭施工工法");      
        }
        if ($('input[name=contact_type_hidden]:eq(2)').prop('checked')) {
            $("#product").html("橋梁施工");      
        }

    } else {

        // 初期クリック
	    $("#contact_type1").click();
        $("#contact_type_hidden-1").click();
	    $("#area1_type1").click();
        $("#area_type_hidden-1").click();
        disable_area2();
        up_hokkaido();
        hover1();

    }

    $("#contact_type1").on("click",function(){
        $("input[name='area2_type']").attr("checked", false);
        disable_area2();
        enable_area1();
        $("#area1_type1").click();
        $("#contact_type_hidden-1").click();
        $("#product").html("工場製品");
        hover1();

    });
    $("#contact_type2").on("click",function(){
        $("input[name='area1_type']").attr("checked", false);
        disable_area1();
        enable_area2();
        $("#area2_type1").click();
        $("#contact_type_hidden-2").click();
        $("#product").html("杭施工工法");      
        hover2();
    });
    $("#contact_type3").on("click",function(){
        disable_area1();
        disable_area2();
        $("input[name='area1_type']").attr("checked", false);
        $("input[name='area2_type']").attr("checked", false);
        $("#contact_type_hidden-3").click();
        $("#product").html("橋梁施工");
        $("input[name='area_type_hidden']").attr("checked", false); 
        $("#area").html("");
        up_kyouryou();
        hover3();

    });

    $("#area1_type1,#area2_type1").on("click",function(){
        up_hokkaido();
        $("#area_type_hidden-1").click();
    });
    $("#area1_type2,#area2_type2").on("click",function(){
        up_touhoku();
        $("#area_type_hidden-2").click();
    });
    $("#area1_type3,#area2_type3").on("click",function(){
        up_kantou();
        $("#area_type_hidden-3").click();
    });
    $("#area1_type4,#area2_type4").on("click",function(){
        up_etc();
        $("#area_type_hidden-4").click();
    });


    });

    function up_hokkaido() {
        $("#area").html("<span class='red'>" + c_hokkaido_name + "</span>へ");
        $("#tel").html(c_hokkaido_tel);
        $("#time").html(c_hokkaido_time);
        $("#mailto").val(c_hokkaido_mail);
    }
    function up_touhoku() {
        $("#area").html("<span class='red'>" + c_touhoku_name + "</span>へ");      
        $("#tel").html(c_touhoku_tel);
        $("#time").html(c_touhoku_time);
        $("#mailto").val(c_touhoku_mail);
    }
    function up_kantou() {
        $("#area").html("<span class='red'>" + c_kantou_name + "</span>へ");      
        $("#tel").html(c_kantou_tel);
        $("#time").html(c_kantou_time);
        $("#mailto").val(c_kantou_mail);
    }
    function up_etc() {
        $("#area").html("<span class='red'>" + c_etc_name + "</span>へ");      
        $("#tel").html(c_etc_tel);
        $("#time").html(c_etc_time);
        $("#mailto").val(c_etc_mail);
    }
    function up_kyouryou() {
        $("#tel").html(c_kyouryou_tel);
        $("#time").html(c_kyouryou_time);
        $("#mailto").val(c_kyouryou_mail);
    }

    function return_reload() {
        if ($('input[name=contact_type_hidden]:eq(0)').prop('checked') ||
            $('input[name=contact_type_hidden]:eq(1)').prop('checked') ||
            $('input[name=contact_type_hidden]:eq(2)').prop('checked')) {
            return true;
        } else {
            return false;
        }
    }

    function disable_area1() {
        $("#area1_type1").prop("disabled",true);
        $("#area1_type2").prop("disabled",true);
        $("#area1_type3").prop("disabled",true);
        $("#area1_type4").prop("disabled",true);
    }

    function disable_area2() {
        $("#area2_type1").prop("disabled",true);
        $("#area2_type2").prop("disabled",true);
        $("#area2_type3").prop("disabled",true);
        $("#area2_type4").prop("disabled",true);
    }


    function enable_area1() {
        $("#area1_type1").prop("disabled", false);
        $("#area1_type2").prop("disabled", false);
        $("#area1_type3").prop("disabled", false);
        $("#area1_type4").prop("disabled", false);
    }

    function enable_area2() {
        $("#area2_type1").prop("disabled", false);
        $("#area2_type2").prop("disabled", false);
        $("#area2_type3").prop("disabled", false);
        $("#area2_type4").prop("disabled", false);
    }

    function hover1() {
        $("#area1").addClass('hover');
        $("#area2").removeClass('hover');
    }
    function hover2() {
        $("#area1").removeClass('hover');
        $("#area2").addClass('hover');
    }
    function hover3() {
        $("#area1").removeClass('hover');
        $("#area2").removeClass('hover');
    }




