
<footer>
    <div class="wrapper cf">
        <div class="footer_inner cf">
            <div class="footer_left">
                <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/footer_logo.png" alt="金森労働研究所 | Kanamori Labor Research Laboratory"></a>
            </div>

            <!-- footer_contact -->
            <div class="footer_right">
                <nav>
                    <ul>
                      <li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                      <li><a href="<?php bloginfo('url'); ?>/about/">金森労働研究所について</a></li>
                      <li><a href="<?php bloginfo('url'); ?>/seminar_list/">セミナー</a></li>
                      <li><a href="<?php bloginfo('url'); ?>/talk_list/">対談</a></li>
                      <li><a href="<?php bloginfo('url'); ?>/topic_list/">トピック</a></li>
                    </ul>
                </nav>
            </div>
            <!-- footer_right -->
        </div>

        <!-- footer_inner -->
    <p class="copy pt_s pb_s">Copyright &copy; Kanamori Labor Research Laboratory All Rights Reserved.</p>
    </div>
    <!-- wrapper -->
    
</footer>
<p id="page_top"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/pagetop.svg" alt="pagetop"></a></p>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
<?php wp_footer(); ?>
</main>
</div>
<!--outer --> 
</body></html>